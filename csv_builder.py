import datetime as dt

import pandas_datareader.data as web
from matplotlib import style

import panda_handler as ph


import os

def create_csv(name):
    style.use("ggplot")

    start = dt.datetime(2000, 1, 1)
    end = dt.datetime(2020, 5, 5)
    df = web.DataReader(name, "yahoo", start, end)

    id = name + ".csv"

    df.to_csv(id)
    print("CSV Builder added " + id + " to your directory")

    ph.show_from_id(id)


def check_market_id(id):
    if os.stat(id).st_size != 0:

        print("CSV is filled... will continue displaying" + id)
    else:
        print("The given ID is wrong")
