import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import pandas as pd
from mplfinance.original_flavor import candlestick_ohlc

import os


def show():


    # Richtige Directory finden

    cwd = os.getcwd()
    CACHE_DIR = os.path.join(cwd, "result")
    user_file = os.path.join(CACHE_DIR, "custom.csv")
    print("DIR -> " + user_file)
    # Lets go

    df = pd.read_csv(user_file, parse_dates=True, index_col=0)
    df_ohlc = df["Adj Close"].resample("10D").ohlc()
    df_volume = df["Volume"].resample("10D").sum()

    df.index = pd.to_datetime(df.index, unit='s')

    df_ohlc.reset_index(inplace=True)
    df_ohlc["Date"] = df_ohlc["Date"].map(mdates.date2num)

    ax1 = plt.subplot2grid((6, 1), (0, 0), rowspan=5, colspan=1)
    ax2 = plt.subplot2grid((6, 1), (5, 0), rowspan=1, colspan=1, sharex=ax1)
    ax1.xaxis_date()

    candlestick_ohlc(ax1, df_ohlc.values, width=2, colorup="g")
    ax2.fill_between(df_volume.index.map(mdates.date2num), df_volume.values, 0)

    plt.show()
